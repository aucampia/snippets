#!/usr/bin/env bash

script_name="${0}"
script_dirname="$( dirname -- "${0}" )"
script_basename="$( basename -- "${0}" )"

trap_handler()
{
    vlog -1 "\${@} = ${@}"
}

sub_reference()
{
    trap 'trap_handler EXIT' EXIT
    trap 'trap_handler TERM' TERM

    local -a etc_files
    readarray -t -d '' etc_files < <(find /etc/ -mindepth 1 -maxdepth 1 -print0)
    vprun 0 declare -p etc_files

    local -a etc_files
    readarray -t etc_files < <(find /etc/ -mindepth 1 -maxdepth 1 -print)
    vprun 0 declare -p etc_files

    mapfile -t -d '' usr_files < <(find /usr/ -mindepth 1 -maxdepth 1 -print0)
    vprun 0 declare -p usr_files
    IFS=', ' read -r -a split_string <<< "A, B, C, D"
    vprun 0 declare -p split_string
}

main_usage()
{
    [ "${#}" -gt 0 ] && {
        1>&2 echo "ERROR: ${@}"
    }
    1>&2 echo -e "${script_basename} [options] command"

    1>&2 echo -e "commands:"

    local -a format=( printf " %-32s%s\n" )
    1>&2 "${format[@]}" "reference" "Say hi"

    1>&2 echo -e "options:"

    local -a format=( printf " %-32s%s\n" )
    1>&2 "${format[@]}" "-h" "help"
    1>&2 "${format[@]}" "-v" "increase verbosity"
    1>&2 "${format[@]}" "-p" "pretend mode"
    return 0;
}

main()
{
    local argv=( "${@}" )
    local verbosity=-1
    local verbose=false
    local pretend=false
    local something=""

    local OPTARG OPTERR="" option OPTIND
    while getopts "hvps:" option "${@}"
    do
        case "${option}" in
            h)
                "${FUNCNAME}_usage"
                return 0;
                ;;
            v)
                verbosity=$(( ${verbosity} + 1 ))
                ;;
            p)
                pretend=true
                ;;
            *)
                "${FUNCNAME}_usage" "Invalid options [ OPTARG=${OPTARG}, OPTERR=${OPTERR}, option=${option} ]"
                return 1
                ;;
        esac
    done
    shift $(( ${OPTIND} - 1 ))

    vrun 0 true && verbose=true

    vprun 0 declare -p script_name script_dirname script_basename argv
    vprun 0 declare -p option OPTERR OPTIND
    vprun 0 declare -p verbosity pretend verbose
    vlog 0 "\${#} == ${#} \${#argv[@]} = ${#argv[@]}"

    vlog 0 "entry ..."

    : <<'MULTILINE_COMMENT'

    [ "${#}" -lt 1 ] && {
        "${FUNCNAME}_usage" "Need command ..."
        return 1
    }
    local command=""
    command="${1}"; shift 1;

    vprun 0 declare -p command

    case "${command}" in
        reference)
                sub_"${command}" "${@}"; return "${?}"
            ;;
        *)
            "${FUNCNAME}_usage" "Invalid command ${command}"
            return 1
            ;;
    esac


MULTILINE_COMMENT

    sub_reference

    for ((i=1;i<=END;i++)); do
        echo $i
    done

    return 0
}

vrun() {
    local level="${1}"; shift 1;
    [ "${verbosity:--1}" -ge "${level}" ] && { "${@}"; return "${?}"; }
    return 0;
}

dump_command()
{
    for arg in "${@}"; do
        printf "%q " "${arg}"
    done
    echo
}

pretend_run()
{
    if "${pretend}"; then
        dump_command "${@}" 1>&2
    else
        "${@}"
    fi
}

vlog() {
    local level="${1}"; shift 1;
    [ "${verbosity:--1}" -ge "${level}" ] && {
        1>&2 echo "$(date +%Y%m%d%H%M%S) ${script_basename}:${FUNCNAME[1]} ${$}" "${@}"
    }
    return 0;
}

vprun() {
    local level="${1}"; shift 1;
    [ "${verbosity:--1}" -ge "${level}" ] && {
        prefix="$(date +%Y%m%d%H%M%S) ${script_basename}:${FUNCNAME[1]} ${$}"
        "${@}" | sed 's|.*|'"${prefix}"' &|g' 1>&2;
        return "${PIPESTATUS[0]}";
    }
    return 0;
}

if ! $(return >/dev/null 2>&1)
then
    script_dirname=$( dirname "${0}" )
    script_basename=$( basename "${0}" )
    main "${@}"
    exit "${?}"
fi
