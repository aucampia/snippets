#!/usr/bin/env bash -x
# https://github.com/containers/buildah/blob/master/examples/lighttpd.sh
# https://github.com/containers/buildah/tree/master/docs
# https://github.com/containers/buildah/blob/master/docs/buildah.md

: ${base_image_registry:=registry}
: ${base_image_name:=name}
: ${base_image_tag:=latest}
: ${base_image:=${base_image_registry}/${base_image_name}:${base_image_tag}}

1>&2 declare -p base_image_registry base_image_name base_image_tag

# https://github.com/containers/buildah/blob/master/docs/buildah-from.md
ctr1=$(buildah from --isolation rootless ${base_image})

1>&2 declare -p ctr1

: ${cache_enabled:=true}

# https://github.com/containers/buildah/blob/master/docs/buildah-run.md
buildah run "$ctr1" -- bash -c <<EOF
exit ${?}
EOF
