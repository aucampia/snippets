#pragma once
#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <execinfo.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// curl https://gitlab.com/aucampia/snippets-000/raw/master/c/debug.h -o /var/tmp/debug.h
// vimdiff <(curl --silent https://gitlab.com/aucampia/snippets-000/raw/master/c/debug.h) /var/tmp/debug.h
// curl https://gitlab.com/aucampia/snippets-000/raw/master/c/debug.h -o debug.h
// vimdiff <(curl --silent https://gitlab.com/aucampia/snippets-000/raw/master/c/debug.h) debug.h
// ln -rs $(readlink -f ./debug.h) debug.h
// ln -s $(readlink -f ./debug.h) debug.h
// ln -s $(readlink -f ./debug.h) /var/tmp/debug.h
// ln -rs "${HOME}/d/gitlab.com/aucampia/snippets-000/c/debug.h" debug.h
// ln -s "${HOME}/d/gitlab.com/aucampia/snippets-000/c/debug.h" debug.h
// #include "/var/tmp/debug.h"
// DEBUG_PRINTF("[this = %p]:entry", this);

inline void debug_print_stack(size_t frames) {
	void *array[frames];
	size_t size;
	char **strings;
	size_t i;

	size = backtrace(array, frames + 1);
	strings = backtrace_symbols(array, size);

	//fprintf (stderr, "Obtained %zd stack frames.\n", size);
	for (i = 1; i < size; i++) {
		fprintf(stderr, "%s\n", strings[i]);
	}

	free(strings);
}

#define DEBUG_VARFS(fspec, var) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%y%m%dT%H%M%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%03li [%06u:%06u:%010u] DEBUG_VAR [%s:%i:%s] [ (%s:%04lu) %s = " fspec " ]\n", \
            date_time_buf, l_timespec.tv_nsec / 1000000, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            __FILENAME__, __LINE__, __PRETTY_FUNCTION__, \
            typeid(var).name(), sizeof(var), #var, var); \
        write(fileno(stderr), __db_buffer, written); \
    }

#define DEBUG_PRINTF(format, ...) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%y%m%dT%H%M%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%03li [%06u:%06u:%010u] DEBUG_VAR [%s:%i:%s] " format "\n", \
            date_time_buf, l_timespec.tv_nsec / 1000000, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            __FILENAME__, __LINE__, __PRETTY_FUNCTION__, \
            __VA_ARGS__); \
        write(fileno(stderr), __db_buffer, written); \
    }
