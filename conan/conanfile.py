#!/usr/bin/env python3


"""
"""

available_versions = [
    "4.15",
    "4.14",
    "4.13",
]

"""
https://docs.conan.io/en/latest/

https://docs.conan.io/en/latest/reference/conanfile.html
https://docs.conan.io/en/latest/reference/conanfile/attributes.html
https://docs.conan.io/en/latest/reference/conanfile/methods.html
https://docs.conan.io/en/latest/reference/conanfile/other.html

https://docs.conan.io/en/latest/reference/tools.html
https://docs.conan.io/en/latest/reference/build_helpers.html
https://docs.conan.io/en/latest/reference/build_helpers/autotools.html

https://docs.conan.io/en/latest/reference/commands/creator/create.html
https://docs.conan.io/en/latest/reference/commands/creator/upload.html

https://docs.conan.io/en/latest/developing_packages.html
https://docs.conan.io/en/latest/developing_packages/package_dev_flow.html

https://github.com/conan-io/examples
https://github.com/conan-io/examples/blob/master/features/package_development_flow/conanfile.py
"""

class TheConan(conans.ConanFile):
    name = "libxfce4util"
    version = [v for v in available_versions if "-" not in v][0]
    """
    https://docs.conan.io/en/latest/mastering/conditional.html
    https://docs.conan.io/en/latest/reference/config_files/settings.yml.html
    https://docs.conan.io/en/latest/extending/custom_settings.html
    https://docs.conan.io/en/latest/reference/profiles.html
    """
    settings = { "os": [ "Linux" ], "arch": [ "x86_64" ] }
    build_policy = "missing"


def direct_main():
    pass

if __name__ == "__main__":
    direct_main()
