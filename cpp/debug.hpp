#pragma once
#include <sys/types.h>
#include <unistd.h>
#include <functional>
#include <sstream>
#include <cstring>
#include <cassert>
#include <execinfo.h>

#define __FILENAME__ (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)

// curl https://gitlab.com/aucampia/snippets-000/raw/master/cpp/debug.hpp -o /var/tmp/debug.hpp
// vimdiff <(curl --silent https://gitlab.com/aucampia/snippets-000/raw/master/cpp/debug.hpp) /var/tmp/debug.hpp
// curl https://gitlab.com/aucampia/snippets-000/raw/master/cpp/debug.hpp -o debug.hpp
// vimdiff <(curl --silent https://gitlab.com/aucampia/snippets-000/raw/master/cpp/debug.hpp) debug.hpp
// ln -rs $(readlink -f ./debug.hpp) debug.hpp
// ln -s $(readlink -f ./debug.hpp) debug.hpp
// ln -s $(readlink -f ./debug.hpp) /var/tmp/debug.hpp
// ln -rs "${HOME}/d/gitlab.com/aucampia/snippets-000/cpp/debug.hpp" debug.hpp
// ln -s "${HOME}/d/gitlab.com/aucampia/snippets-000/cpp/debug.hpp" debug.hpp
// #include "/var/tmp/debug.hpp"
// DEBUG_PRINTF("[this = %p]:entry", this);

namespace debug {

template <typename T>
std::string dump_sequence(const T& in) {
    std::stringstream out;
    size_t next_index = 0;
    for ( auto item : in ) {
        size_t index = next_index++;
        if ( index != 0 ) out << ", ";
        out << "[" << index << "]" << " -> " << item;
    }
    return out.str();
}

template <typename T>
std::string stringify(const T& in) {
    std::stringstream out;
    out << in;
    return out.str();
}

template <typename T>
const char* cstringify(const T& in) {
    return stringify(in).c_str();
}

inline void writer(int fd, const char* message, size_t length) {
    ::write(fd, message, length);
}

//extern std::function<void(int, const char*, size_t length)> writer;

inline void print_stack(size_t frames = 100) {
    void *array[frames + 10];
    size_t size;
    char **strings;
    size_t i;

    size = backtrace(array, frames + 1);
    strings = backtrace_symbols(array, size);

    //fprintf (stderr, "Obtained %zd stack frames.\n", size);
    for (i = 1; i < size; i++) {
        fprintf(stderr, "%s\n", strings[i]);
    }

    free(strings);
}

template< typename functor_type >
class sentry {
    functor_type functor;
public:
    sentry(functor_type in_functor) : functor( std::move( in_functor ) ) {}

    sentry(sentry &&) = delete;
    sentry(sentry const &) = delete;

    ~sentry() noexcept {
        static_assert( noexcept( functor() ),
            "Please check that the finally block cannot throw, "
            "and mark the lambda as noexcept." );
        functor();
    }
};

template< typename functor_type >
sentry< functor_type > finally( functor_type functor ) { return { std::move( functor ) }; }

/*
  auto&& finally = debug::finally([&]() noexcept {
    DEBUG_PRINTF(":this = %p:finally ...", this);
  });
*/

}

#define DEBUG_VAR(var) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%y%m%dT%H%M%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%03li [%06u:%06u:%010u] DEBUG_VAR [%s:%i:%s] [ (%s:%04lu) %s = %s ]\n", \
            date_time_buf, l_timespec.tv_nsec / 1000000, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            __FILENAME__, __LINE__, __PRETTY_FUNCTION__, \
            typeid(var).name(), sizeof(var), #var, debug::stringify((var)).c_str()); \
        debug::writer(fileno(stderr), __db_buffer, written); \
    }

#define DEBUG_ASSERT(var) \
    { \
        DEBUG_VAR(var); \
        assert(var); \
    } \

#define DEBUG_VARFS(fspec, var) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%y%m%dT%H%M%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%03li [%06u:%06u:%010u] DEBUG_VAR [%s:%i:%s] [ (%s:%04lu) %s = " fspec " ]\n", \
            date_time_buf, l_timespec.tv_nsec / 1000000, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            __FILENAME__, __LINE__, __PRETTY_FUNCTION__, \
            typeid(var).name(), sizeof(var), #var, var); \
        debug::writer(fileno(stderr), __db_buffer, written); \
    }

#define DEBUG_PRINTF(format, ...) \
    { \
        struct timespec l_timespec; \
        clock_gettime(CLOCK_REALTIME, &l_timespec); \
\
        struct tm l_tm; \
        localtime_r(&(l_timespec.tv_sec), &l_tm); \
\
        char date_time_buf[32]; \
        strftime(date_time_buf, 32, "%y%m%dT%H%M%S", &l_tm); \
\
        char __db_buffer[8 * 1024]; \
\
        int written = snprintf(__db_buffer, 8 * 1024, "%s.%03li [%06u:%06u:%010u] DEBUG_VAR [%s:%i:%s] " format "\n", \
            date_time_buf, l_timespec.tv_nsec / 1000000, \
            (unsigned int)(getpid()), (unsigned int)(getppid()), (unsigned int)(pthread_self()), \
            __FILENAME__, __LINE__, __PRETTY_FUNCTION__, \
            __VA_ARGS__); \
        debug::writer(fileno(stderr), __db_buffer, written); \
    }
