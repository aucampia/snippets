# ...

```bash
docker-compose -f shaij/docker-compose.yml config
docker-compose -f shaij/docker-compose.yml up --detach

docker-compose -f shaij/docker-compose.yml ps

docker-compose -f shaij/docker-compose.yml rm --stop
docker-compose -f shaij/docker-compose.yml logs -f

socat STDIO TCP4:localhost:24592,connect-timeout=5
```
