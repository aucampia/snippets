# ...

```bash
regen-from-svg.sh -v
svg-ppi-unit.sh 96 cm *.svg | column -t
svg-ppi-unit.sh 96 in *.svg | column -t


```

```bash
## Width x Height

pip3 install --user --upgrade pint

# 96 dpi

## Text Area
# 14.900 in (37.846 cm) --[ 96 dpi ]--> 1430 px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(14.9 * 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(37.846, "cm").to("in").m * 96)'
# 6.150 in (15.621 cm) --[ 96 dpi ]--> 590 px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(6.15 * 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(15.621, "cm").to("in").m * 96)'

## Larger Area
# 14.900 in (37.846 cm) --[ 96 dpi ]--> 1430 px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(14.900 * 96)'
# 6.980 in (17.73 cm) --[ 96 dpi ]--> 670 px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(6.980 * 96)'

## Page
# 16.000 in (40.640 cm) --[ 96 dpi ]--> 1536px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(16 * 96)'
# 9.000 in (22.860 cm) --[ 96 dpi ]--> 864px
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(9 * 96)'
```

```bash
# 1430 px --[ 96 dpi ]--> 14.896 in (37.835 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(1430 / 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(1430 / 96, "in").to("cm").m)'
# 590 px --[ 96 dpi ]--> 6.146 in (15.610 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(590 / 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(590 / 96, "in").to("cm").m)'

## 14.896 x 6.146 in @ 96 dpi = 1430 x 590 px
## 37.835 x 15.610 cm @ 96 dpi = 1430 x 590 px


# 1430 px --[ 96 dpi ]--> 14.896 in (37.835 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(1430 / 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(1430 / 96, "in").to("cm").m)'
# 670 px --[ 96 dpi ]--> 6.979 in (17.727 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(670 / 96)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(670 / 96, "in").to("cm").m)'

## 14.896 x 6.979 in @ 96 dpi = 1430 x 670 px
## 37.835 x 17.727 cm @ 96 dpi = 1430 x 670 px

# 1530 px --[ 96 dpi ]--> 15.938 in (40.481 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(1530 / 96)'
# 860 px --[ 96 dpi ]--> 8.958 in (22.754 cm)
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(860 / 96)'
```

## OLD

```bash
# 37.84 cm --[ 96 dpi ]--> 1430 px
# 15.62 cm --[ 96 dpi ]--> 590 px

# 1430 px --[ 96 dpi ]--> 37.835 cm
# 670 px --[ 96 dpi ]--> 17.727 cm


python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(17.73, "cm").to("in").m)'
python3 -c 'import pint; ureg = pint.UnitRegistry(); print(ureg.Quantity(17.73, "in").to("cm").m)'
```

## ...

* https://app.diagrams.net/?src=about#Aaucampia%2Fsnippets%2Fmaster%2Fdraw.io%2Fc4-template-a4-nohtml.drawio
* https://app.diagrams.net/?src=about#Aaucampia%2Fsnippets%2Fmaster%2Fdraw.io%2Fc4-template-a3-nohtml.drawio
