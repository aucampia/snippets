# vim: set noexpandtab fo-=t:
# https://www.gnu.org/software/make/manual/make.html
.PHONY: all
all:

########################################################################
# boiler plate
########################################################################
SHELL=bash
.SHELLFLAGS=-ec -o pipefail

current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

ifneq ($(filter all vars,$(VERBOSE)),)
dump_var=$(info var $(1)=$($(1)))
dump_vars=$(foreach var,$(1),$(call dump_var,$(var)))
else
dump_var=
dump_vars=
endif

ifneq ($(filter all targets,$(VERBOSE)),)
__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)
endif

$(call dump_vars,current_makefile current_makefile_dirname \
	current_makefile_dirname_abspath current_makefile_dirname_realpath)

########################################################################
# code here ...
########################################################################

.PHONY: clean
clean:

.PHONY: nothing
nothing:
	@echo doing $(@) ...

########################################################################
# boiler plate
########################################################################




__empty:=
__space:=$(__empty) $(__empty)

define __newline


endef


true:=T
false:=
seq = $(if $(subst x$(1)x,,x$(2)x)$(subst x$(2)x,,x$(1)x),$(false),$(true))

# asserts that expected $(1) matches actual $(2)
assert=$(if $(call seq,$(1),$(2)),$(1),$(error expected output $(1) did not match actual output $(2)))
#
reflex_path=$(subst $(__space),/,$(foreach dir,$(subst /,$(__space),$(1)),..))
# $1 = dirs $2 = patterns
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
#rwildcard=$(wildcard $(foreach p,$(2),$1$p)) $(foreach d,$(wildcard $1/*/),$(call rwildcard,$d/,$2))

########################################################################
# useful ...
########################################################################

.PHONY: help
help: ## Show this message.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'


## force ...
.PHONY: .FORCE
.FORCE:
$(force_targets): .FORCE

## dirs ...
.PRECIOUS: %/
%/:
	mkdir -vp $(@)

#	{ test -d $(*) && rm -vr $(*); } || echo "$(@): $(*) does not exist ..."
.PHONY: clean-%/
clean-%/:
	@{ test -d $(*) && { set -x; rm -vr $(*); set +x; } } || echo "directory $(*) does not exist ... nothing to clean"

## stamps
.PHONY: .FORCE
__refresh_stamps_target=$(if $(refresh_stamps),.FORCE,)

%.stamp: $(__refresh_stamps_target)
	echo stamp > $(@)

%.tstamp: $(__refresh_stamps_target)
	date +%Y-%m-%dT%H:%M:%S > $(@)

.PHONY: clean-stamp
clean: clean-stamp
clean-stamp:
	rm -v *.stamp *.tsstamp || true

