ifdef EXPORT_ALL
.EXPORT_ALL_VARIABLES:
endif

.PHONY: all
all:
	@echo
	@echo Test Summary
	@echo ------------
	@echo "$(call int_decode,$(passed)) tests passed; $(call int_decode,$(failed)) tests failed"

include iaxufm.mk

passed :=
failed :=

ECHO := /bin/echo

start_test = $(if $0,$(shell $(ECHO) -n "Testing '$1': " >&2))$(eval current_test := OK)
stop_test  = $(if $0,$(shell $(ECHO) " $(current_test)" >&2))
test_pass = .$(eval passed := $(call int_inc,$(passed)))$(info '$1' == '$2')
test_fail = X$(eval failed := $(call int_inc,$(failed)))$(eval current_test := ERROR '$1' != '$2')
test_assert = $(if $0,$(if $(filter undefined,$(origin 2)),$(eval 2 :=))$(shell $(ECHO) -n $(if $(call seq,$1,$2),$(call test_pass,$1,$2),$(call test_fail,$1,$2)) >&2))

$(call start_test,reflex_path)
$(call test_assert,$(call reflex_path,/aa),../)
$(call test_assert,$(call reflex_path,/aa/bb/cc/dd/ee/ff),../)
$(call stop_test)
