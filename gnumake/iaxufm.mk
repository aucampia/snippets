# from gmsl

true:=T
false:=
seq = $(if $(subst x$(1)x,,x$(2)x)$(subst x$(2)x,,x$(1)x),$(false),$(true))

# asserts that expected $(1) matches actual $(2)
assert=$(if $(call seq,$(1),$(2)),$(1),$(error expected output $(1) did not match actual output $(2)))
#
reflex_path=$(subst $(__space),/,$(foreach dir,$(subst /,$(__space),$(1)),..))
# $1 = dirs $2 = patterns
rwildcard=$(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))
