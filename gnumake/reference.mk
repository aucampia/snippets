# vim: set noexpandtab fo-=t:

# https://www.gnu.org/software/make/manual/make.html

########################################################################
# boiler plate
########################################################################

.PHONY: all help
all:
help:

SHELL=bash
current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

__empty:=
__space:=$(__empty) $(__empty)

define __newline


endef

ifneq ($(filter all vars,$(VERBOSE)),)
dump_var=$(info var $(1)=$($(1)))
dump_vars=$(foreach var,$(1),$(call dump_var,$(var)))
else
dump_var=
dump_vars=
endif

ifneq ($(filter all targets,$(VERBOSE)),)
__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)
endif

$(call dump_vars,current_makefile current_makefile_dirname \
	current_makefile_dirname_abspath current_makefile_dirname_realpath)

########################################################################
# useful ...
########################################################################

## dirs ...
.PRECIOUS: %/
%/:
	mkdir -vp $(@)

.PHONY: clean-%/ clean
clean-%/:
	{ test -d $(*) && rm -vr $(*); } || echo "$(@): directory $(*) does not exist (doing nothing)"

## always ...
.PHONY: .ALWAYS
.ALWAYS:

## force ...
.PHONY: .FORCE
.FORCE:
$(force_targets): .FORCE

## stamps
.PHONY: .FORCE
__refresh_stamps_target=$(if $(refresh_stamps),.FORCE,)

%.stamp: $(__refresh_stamps_target)
	echo stamp > $(@)

%.tstamp: $(__refresh_stamps_target)
	date +%Y-%m-%dT%H:%M:%S > $(@)

.PHONY: clean-stamp clean
clean: clean-stamp
clean-stamp:
	rm -v *.stamp *.tsstamp || true

## functions ...
reflex_path=$(subst $(__space),/,$(foreach dir,$(subst /,$(__space),$(1)),..))

true:=T
false:=
seq = $(if $(subst x$(1)x,,x$(2)x)$(subst x$(2)x,,x$(1)x),$(false),$(true))
assert=$(if $(call seq,$(1),$(2)),$(1),$(error expected output $(2) did not match actual output $(1)))

########################################################################
# local ...
########################################################################

build_dir=build
clean: clean-$(build_dir)/

########################################################################
# targets ...
########################################################################

help:
	@printf "########################################################################\n"
	@printf "TARGETS:\n"
	@printf "########################################################################\n"
	@printf "  %-32s%s\n" "help" "this output ..."
	@printf "  %-32s%s\n" "all" "default target ..."
	@printf "########################################################################\n"
	@printf "VARIABLES:\n"
	@printf "########################################################################\n"
	@printf "  %-32s%s\n" "VERBOSE" "Set to make things more verbose (all, vars, targets)"

all:
	@echo use specifc target ...

.PHONY: nothing
nothing:
	@echo doing $(@) ...

.PHONY: dummy-%
dummy-%:
	@echo DUMMY $(*)

.PHONY: say-%
say-%: dummy-%
	@echo I am saying $(*)

some.stamp: other.stamp

path_a=dir_a/dir_b/dir_c/
path_b=dir_a/dir_b/dir_c
path_c=dir_a
words_a=word_a word_b word_c word_d
filenames_a=file_a.ext file_b.ext file_c.ext

reflex_path=$(subst $(__space),/,$(foreach dir,$(subst /,$(__space),$(1)),..))

string_functions:
	@echo $(call assert,$(filenames_a:file_%.ext=aaa_%.zzz),aaa_a.zzz aaa_b.zzz aaa_c.zzz)
	@echo $(patsubst file_%.ext,aaa_%.zzz,$(filenames_a))
	@echo $(subst d_,,$(words_a))
	@echo $(filenames_a:.ext=.yyy)
	@echo $(patsubst .ext,.yyy,$(filenames_a))
	@echo $(call assert,$(subst /, ,$(path_a)),dir_a dir_b dir_c )
	@echo $(call assert,$(subst /, ,$(path_b)),dir_a dir_b dir_c)
	@echo $(call assert,$(call reflex_path,$(path_a)),../../..)
	@echo $(call assert,$(call reflex_path,$(path_b)),../../..)
