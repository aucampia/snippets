current_makefile:=$(lastword $(MAKEFILE_LIST))
current_makefile_dirname:=$(dir $(current_makefile))
current_makefile_dirname_abspath:=$(dir $(abspath $(current_makefile)))
current_makefile_dirname_realpath:=$(dir $(realpath $(current_makefile)))

PRETEND=T

pretend_echo=$(if $(PRETEND),echo,)
pretend_nop=$(if $(PRETEND),true,)

d_dir=$(current_makefile_dirname_abspath)/d

link-dirs:
	#find d/ -name .stwlink -printf '%h\n'
	find d/ -name .stwlink -printf '%h\0' | xargs -I {} -0 dirname {} | sort | uniq | tr '\n' '\000' | xargs -0 -I{} -t $(pretend_nop) mkdir -vp ~/{}
	find d/ -name .stwlink -printf '%h\0' | xargs -I {} -0 -t $(pretend_nop) ln -T -vrs {} ~/{}

link-git:
	#find d/ -name .git -printf '%h\n'
	find d/ -name .git -printf '%h\0' | xargs -I {} -0 dirname {} | sort | uniq | tr '\n' '\000' | xargs -0 -I{} -t $(pretend_nop) mkdir -vp ~/{}
	find d/ -name .git -printf '%h\0' | xargs -I {} -0 -t $(pretend_nop) ln -T -vrs {} ~/{}

link: link-dirs link-git

changeecho-owner:
	find . -name 'echo' -printf '%h\000' | xargs -t -0 -n1 bash -c 'git -C "${1}" config user.name "Iwan Aucamp"; git -C "${1}" config user.email "aucampia@gmail.com"' /dev/null

ignore:
	echo '#include shared-stignore' > .stignore

activate: link

git-status:
	find ./d/ -name .git -printf '%h\n' | xargs -t -I{} git -C {} status --short

git-pull:
	find ./d/ -name .git -printf '%h\n' | xargs -t -I{} git -C {} pull

git-push:
	find ./d/ -name .git -printf '%h\n' | xargs -t -I{} git -C {} push

git-candp:
	find ./d/ -name .git -printf '%h\n' | xargs -I{} echo git -C {} candp -- -m "updating..."
