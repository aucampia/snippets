# vim: set noexpandtab fo-=t:

__ORIGINAL_SHELL:=$(SHELL)
SHELL=$(warning Building $@$(if $<, (from $<))$(if $?, ($? newer)))$(TIME) $(__ORIGINAL_SHELL)

all: aa.stamp ba.stamp

%.stamp:
	echo stamp > $(@)

stamp-clean:
	rm -vf *.stamp

clean: stamp-clean

.PHONY: %.phony
%.phony:
	echo $(@)

aa.stamp: ab.stamp
ab.stamp: ac.stamp

ba.stamp: bb.stamp
bb.stamp: bc.phony
