# ...

```
The author(s) of this file has made it available under any of the following terms:
 - CC0-1.0: https://spdx.org/licenses/CC0-1.0.html
 - MIT-0: https://spdx.org/licenses/MIT-0.html

 - CC0-1.0: https://creativecommons.org/publicdomain/zero/1.0/
 - MIT-0: https://github.com/aws/mit-0

SPDX-License-Identifier: CC0-1.0

The author(s) of this file has made it available under CC0-1.0
https://spdx.org/licenses/CC0-1.0.html
```
