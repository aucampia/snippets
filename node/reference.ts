#!/usr/bin/env ts-node-script

/*
npm install @types/node
npm install -g ts-node
npm install -g typescript
*/

/*
https://www.typescriptlang.org/docs/handbook/
*/

declare let something:any;

import consolem from 'console';

import path from 'path';

const __file__ = path.basename(__filename);

global.console = new consolem.Console(process.stdout, process.stderr);

// typedef

type Data = string | Buffer;

// https://www.typescriptlang.org/docs/handbook/release-notes/typescript-3-0.html#generic-rest-parameters

function wrapC<T extends any[], U>(fn: (...args: T) => U) {
  const __func__ = `${__file__}:WrapC`
  console.info(`${__func__}:entry`);
  return (...args: T) => {
    console.info(`${__func__}:inner:before:${fn.name}:args = `, args);
    const result = fn(...args);
    console.info(`${__func__}:inner:after:${fn.name}:result = `, result);
    return result;
  }
}

function wrapPromiseA<T extends Array<any>, U extends Data>(fn: (...args: T) => Promise<U>) {
  const __func__ = `${__file__}:WrapPromiseA`
  console.info(`${__func__}:entry`);
  return async (...args: T) => {
    console.info(`${__func__}:inner:before:${fn.name}:args = `, args);
    await dummyAsync(`${fn.name}: before ...`);
    const result = await fn(...args);
    await dummyAsync(`${fn.name}: after ...`);
    console.info(`${__func__}:inner:after:${fn.name}:result = `, result);
    return result;
  }
}

function dummyPlain(arg: string): string {
  const __func__ = `${__file__}:dummyPlain`
  console.info(`${__func__}:entry:${arg}`);
  return `${__func__}:${arg}`;
};

const dummyArrow = (arg:string): string => {
  const __func__ = `${__file__}:dummyArrow`
  console.info(`${__func__}:entry:${arg}`);
  return `${__func__}:${arg}`;
};

async function dummyAsync(arg: string): Promise<string> {
  const __func__ = `${__file__}:dummyAsync`
  console.info(`${__func__}:entry:${arg}`);
  return `${__func__}:${arg}`;
}

function dummyPromise(arg: string): Promise<string> {
  const __func__ = `${__file__}:dummyPromise`
  // same as return Promise.resolve(`${__func__}:${arg}`);
  return new Promise((resolve, reject) => {
    console.info(`${__func__}:entry:${arg}`);
    resolve(`${__func__}:${arg}`);
  });
}

function sleep(ms:number) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

function useSleep() {
  let foo = "dogs";
  sleep(5).then(()=>{
    console.log(`here comes the ...`);
    console.log(`foo = ${foo}`);
  });
  console.log(`calling all ...`);
  console.log(`foo = ${foo}`);
  foo = "cats";
}

// https://github.com/piotrwitek/utility-types/blob/master/src/mapped-types.ts
export type OmitByValue<T, ValueType> = Pick< T, { [Key in keyof T]-?: T[Key] extends ValueType ? never : Key }[keyof T] >;


//https://www.typescriptlang.org/docs/handbook/classes.html#parameter-properties
//https://github.com/microsoft/TypeScript/issues/26792
//https://www.typescriptlang.org/docs/handbook/compiler-options.html#strictPropertyInitialization
class DataClassA {
  id!: string;
  valA!: string;
  valB!: number;
  valC?: string;
  //constructor(data: OmitByValue<DataClassA, Function>) { return Object.assign(this, data) as DataClassA; }
  constructor(data: OmitByValue<DataClassA, Function>) { Object.assign(this, data); }
  dummy(arg: string) {
    return `${this.id}:${this.valA}:${this.valB}:${arg}`;
  }
}

async function main() {
  const __func__ = `${__file__}:main:`
  console.info(`${__func__}:entry`);
  console.info(`wrapB: `, (wrapC(dummyArrow))("S") + "X")
  console.info(`wrapB: `, await wrapC(dummyAsync)("S") + "X")
  console.info(`wrapB: `, await wrapC(dummyPromise)("S") + "X")
  console.info(`wrapB: `, await wrapPromiseA(dummyAsync)("S") + "X")
  console.info(`wrapB: `, await wrapPromiseA(dummyPromise)("S") + "X")
  {
    console.debug(`val = `, new DataClassA({id: "0", valA: "1", valB: 2}));
  }
}

main().catch((error) => {
  console.error(`error = `, error);
  process.exitCode = 1;
});

/*
TRASH:

  console.info(`wrapA: `, (wrapA(dummyPlain))("S") + "X")
  console.info(`wrapB: `, (wrapB(dummyArrow))("S") + "X")
  console.info(`wrapB: `, await wrapB(dummyAsync)("S") + "X")
  console.info(`wrapB: `, await wrapB(dummyPromise)("S") + "X")
function wrapA<T extends Function>(fn: T): T {
  const __func__ = `${__file__}:WrapA`
  console.info(`${__func__}:entry`);
  return <any>function(...args: any) {
    console.info(`${__func__}:inner:before:${fn.name}:args = `, args);
    const result = fn(...args);
    console.info(`${__func__}:inner:after:${fn.name}:result = `, result);
    return result;
  }
}

function wrapB<T extends Array<any>, U>(fn: (...args: T) => U) {
  const __func__ = `${__file__}:WrapB`
  console.info(`${__func__}:entry`);
  return (...args: T) => {
    console.info(`${__func__}:inner:before:${fn.name}:args = `, args);
    const result = fn(...args);
    console.info(`${__func__}:inner:after:${fn.name}:result = `, result);
    return result;
  }
}

*/
