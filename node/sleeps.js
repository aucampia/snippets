
function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function main() {
  let foo = "dogs";
  sleep(10).then(()=>{
    console.log(`here comes the ...`);
    console.log(`foo = ${foo}`);
  });
  console.log(`calling all ...`);
  console.log(`foo = ${foo}`);
  foo = "cats";
}

main().catch((error) => {
  console.error(`error = `, error);
  process.exitCode = 1;
});
