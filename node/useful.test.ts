
// KeyType extends (number | string), ValueType
// { [key: KeyType]: ValueType }

import fsm from "fs";


export function pathExistsSync(path: string): boolean {
  try {
    fsm.accessSync(path);
    return true;
  } catch (error) {
  }
  return false;
};

export async function pathExists(path: string): Promise<boolean> {
  try {
    await fsm.promises.access(path);
    return true;
  } catch (error) {
  }
  return false;
};


function getKeyValue<ValueType>(obj: { [key: number]: ValueType }, key: number, dflt: ValueType): ValueType
function getKeyValue<ValueType>(obj: { [key: string]: ValueType }, key: string, dflt: ValueType): ValueType
function getKeyValue<ValueType>(obj: { [key: number]: ValueType } | { [key: string]: ValueType }, key: (string | number), dflt?: ValueType): ValueType {
  let result = ( obj == null ? undefined : (obj as any)[key] );
  if ( result != null ) return result;
  if ( dflt != null ) return dflt;
  throw Error(`Could not find ${key} and no default value supplied`);
}


describe("tests", () => {

  test("test getKeyValue", () => {
    const dict = { a: 1, b: 2 };
    expect(getKeyValue(dict, 'a', 99)).toBe(1);
    expect(getKeyValue(dict, 'z', 99)).toBe(99);
  });
  test("test pathExistsSync", () => {
    const dict = { a: 1, b: 2 };
    expect(pathExistsSync("/etc/hosts")).toBe(true);
    expect(pathExistsSync("/etc/oithaanoveengipe")).toBe(false);
  });
  test("test pathExists", async () => {
    const dict = { a: 1, b: 2 };
    expect(await pathExists("/etc/hosts")).toBe(true);
    expect(await pathExists("/etc/oithaanoveengipe")).toBe(false);
  });
});

