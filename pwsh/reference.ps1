#!/usr/bin/env pwsh

<#
    https://en.wikipedia.org/wiki/PowerShell#Comparison_of_cmdlets_with_similar_commands
    https://ss64.com/ps/
    https://docs.microsoft.com/en-us/powershell/
#>

function Main {
    Write-Output "${env:PATH}".Replace(";", "`n").Replace(":", "`n")

}

Main
