# ...

```bash
poetry run black .

poetry run black --check .
poetry run flake8 .
poetry run pytest *.py
poetry run pytest --log-cli-level=debug *.py
poetry run mypy --strict *.py


pipenv run pytest *.py
pipenv run pytest --log-cli-level=debug *.py

pipenv run mypy --strict *.py
pipenv run pylint *.py

grep --exclude-dir={.mypy,.pytest,__pycache}'*' -ri static ./
```
