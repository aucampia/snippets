#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# pylint: disable=bad-continuation

import argparse
import sys
import logging

logging.basicConfig(
    level=logging.INFO,
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)


def main():
    parser = argparse.ArgumentParser(add_help=True)
    parser.add_argument(
        "-v",
        "--verbose",
        action="count",
        dest="verbosity",
        help="increase verbosity level",
    )
    subparsers = parser.add_subparsers(dest="subparser_000", required=True)
    subparsers.add_parser("id")
    parse_result = parser.parse_args(["id"])
    logging.info("parse_result = %s", parse_result)


if __name__ == "__main__":
    main()
