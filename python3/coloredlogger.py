#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# vim: set filetype=python tw=100 cc=+1:

import logging
import sys

LOGGER = logging.getLogger(__name__)

try:
    import coloredlogs
except ImportError:
    coloredlogs = None


def main() -> None:
    if coloredlogs:
        root_logger = logging.getLogger("")
        root_logger.propagate = True
        root_logger.setLevel(logging.INFO)
        handler = logging.StreamHandler(sys.stderr)
        handler.setFormatter(
            coloredlogs.ColoredFormatter(
                datefmt="%Y-%m-%dT%H:%M:%S",
                fmt=(
                    "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
                    "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
                ),
            )
        )
        root_logger.addHandler(handler)
    else:
        logging.basicConfig(
            level=logging.INFO,
            stream=sys.stderr,
            datefmt="%Y-%m-%dT%H:%M:%S",
            format=(
                "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
                "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
            ),
        )


if __name__ == "__main__":
    main()
