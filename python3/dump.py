import functools


def mextends_before(function):
    @functools.wraps(function)
    def wrapper(self, *args, **kwargs):
        base_function = getattr(self.__class__.__mro__[1], function.__name__)
        result = function(self, *args, **kwargs)
        result = base_function(self, *args, **kwargs)
        return result

    return wrapper


def mextends_after(function):
    @functools.wraps(function)
    def wrapper(self, *args, **kwargs):
        base_function = getattr(self.__class__.__mro__[1], function.__name__)
        result = base_function(self, *args, **kwargs)
        result = function(self, *args, **kwargs)
        return result

    return wrapper
