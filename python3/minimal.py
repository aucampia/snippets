import logging
import typing
import os
import sys

logging.basicConfig(
    level=os.environ.get("PYTHON_LOGGING_LEVEL", logging.INFO),
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)


def main() -> None:
    pass


if __name__ == "__main__":
    main()
