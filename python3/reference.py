#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# pylint: disable=bad-continuation

"""
This module contains some examples of docstrings.

# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html
# https://pythonhosted.org/an_example_pypi_project/sphinx.html#full-code-example
# https://docs.python-guide.org/writing/documentation/
# https://devguide.python.org/documenting/
# https://docs.python.org/3/reference/datamodel.html#basic-customization


################################################################################
# Examples
################################################################################
# - https://pythonhosted.org/an_example_pypi_project/sphinx.html#full-code-example

################################################################################
# Shinx Style
################################################################################
# - https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#the-python-domain
# - https://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html#info-field-lists
# - https://www.python.org/dev/peps/pep-0287/

################################################################################
# Google Style
################################################################################
# - https://google.github.io/styleguide/pyguide.html
# - https://www.sphinx-doc.org/en/master/usage/extensions/napoleon.html
# - https://www.sphinx-doc.org/en/master/usage/extensions/example_google.html


################################################################################
# Other
################################################################################
# https://docs.python.org/3/library/stdtypes.html
# https://stackoverflow.com/questions/3051241/how-to-document-class-attributes-in-python
"""

import logging


import typing

import os.path

import pathlib

import sys

# import argparse
import enum

# import inspect
import pprint
import datetime

try:
    import coloredlogs
except ImportError:
    coloredlogs = None

LOGGER: logging.Logger


SCRIPT_DIRNAME = os.path.dirname(__file__)
SCRIPT_DIRNAMEABS = os.path.abspath(SCRIPT_DIRNAME)
SCRIPT_BASENAME = os.path.basename(__file__)

SCRIPT_PATH = pathlib.Path(__file__)

# positional only comes in 3.8
# def function_a(arg_a, /, arg_b, *, arg_c, arg_d=None):


def function_a(
    arg_a: str, arg_b: str, *, arg_c: str, arg_d: typing.Optional[typing.Any] = None
) -> str:
    """
    :param str arg_a: first arg, positional only.
    :param int arg_b: second arg, positional or keyword.
    :param list arg_c: third arg, keyword only.
    :param dict arg_d: fourth arg, keyword only, optional

    :returns: some string ...
    :rtype: str
    """
    return "{}:{}:{}:{}".format(arg_a, arg_b, arg_c, arg_d)


GenericT = typing.TypeVar("GenericT")


class ClassA:
    """
    This is some Class.

    Static Attributes:
        :var str static_attribute_a: A static attribute.
        :var str static_attribute_b: A static attribute.
    """

    static_attribute_a = "static_attribute_a"
    static_attribute_b = "static_attribute_b"
    stats: typing.ClassVar[typing.Dict[str, int]] = {}

    @staticmethod
    def staticmethod_a(arg_a: typing.Any) -> None:
        """"""
        pass

    @classmethod
    def classmethod_a(cls, arg_a: typing.Any) -> None:
        pass

    def __init__(self, arg_a: typing.Any):
        """"""
        self.attribute_a = "attribute_a"
        self._private_attribute_a = "_private_attribute_a"
        self._property_a = "_property_a"

    def method_a(self, arg_a: typing.Any) -> None:
        """"""

    @property
    def property_a(self) -> typing.Any:
        """I'm the 'property_a' property."""
        return self._property_a

    @property_a.setter
    def property_a(self, value: typing.Any) -> None:
        self._property_a = value

    @property_a.deleter
    def property_a(self) -> None:
        del self._property_a

    @classmethod
    def create_instance(cls, arg_a: typing.Any) -> "ClassA":
        return cls(arg_a)


class WithLogger:
    _logger = None

    @classmethod
    def logger(cls) -> logging.Logger:
        if cls._logger is None:
            cls._logger = logging.getLogger(__name__ + "." + cls.__name__)
        return cls._logger

    def __init__(self, *args: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__()


class ClassC(WithLogger):
    def __init__(self, *args: typing.Any, **kwargs: typing.Any):
        super().__init__(*args, **kwargs)
        self.logger().info("args = %s, kwargs = %s", args, kwargs)


class ClassD(WithLogger):
    def __init__(self, *args: typing.Any, **kwargs: typing.Any):
        super().__init__(*args, **kwargs)
        self.logger().info("args = %s, kwargs = %s", args, kwargs)


class ClassE(ClassC, ClassD):
    def __init__(self, *args: typing.Any, **kwargs: typing.Any):
        super().__init__(*args, **kwargs)
        self.logger().info("args = %s, kwargs = %s", args, kwargs)


class ExampleProxyMethods(dict):
    def __init__(self, *args: typing.Any, **kwargs: typing.Any):
        super().__init__(*args, **kwargs)
        LOGGER.info("args = %s, kwargs = %s", args, kwargs)

    def keys(self, *args: typing.Any, **kwargs: typing.Any) -> typing.Iterable[str]:
        LOGGER.info("args = %s, kwargs = %s", args, kwargs)
        return super().keys(*args, **kwargs)


def inspect_data() -> None:
    # dump / inspect / print / output ...
    LOGGER.info("pprint = %s", pprint.pformat({"a": {"b": {"c": 0}}}))


def example_except() -> None:
    try:
        LOGGER.info("here ...")
        raise RuntimeError("This is an example ...")
    except OSError as exception:
        LOGGER.error("caught exception = %s", exception)
        LOGGER.error("caught exception ...", exc_info=True)
        raise
    except:
        exception = sys.exc_info()[0]
        LOGGER.error("caught exception = %s", exception)
        LOGGER.error("caught exception ...", exc_info=True)
    finally:
        LOGGER.info("finally ...")


"""
StaticInitableT = typing.TypeVar("StaticInitableT", bound="StaticInitable")


class StaticInitable(typing.Protocol):
    @classmethod
    def static_init(cls) -> None:
        ...


def static_init(cls: typing.Type[StaticInitable]) -> typing.Type[StaticInitable]:
    if getattr(cls, "static_init", None):
        cls.static_init()
    return cls


@static_init
class SomeEnum(enum.Enum):
    VAL_A = enum.auto()
    VAL_B = enum.auto()
    VAL_C = enum.auto()
    VAL_D = enum.auto()

    @classmethod
    def static_init(cls) -> None:
        setattr(cls, "rmap", {})
        for value in cls:
            typing.cast(typing.Any, cls).rmap[value.name.lower()] = value.value

...
        for xi in SomeEnum:
            LOGGER.info("xi = %s", xi)
        LOGGER.info("SomeEnum.rmap = %s", SomeEnum.rmap)
"""


def reference() -> None:
    try:
        LOGGER.info("entry ...")

        dt_x = datetime.datetime.strptime(
            "2020-04-19", r"%Y-%m-%d"
        ) + datetime.timedelta(weeks=52)
        dt_x

        inspect_data()
        ClassE(1, "2", [1, 2, 3], kwarg_a="1")
        example_except()
        z = ExampleProxyMethods(object="TEST")
        z.keys()

    finally:
        LOGGER.info("finally ...")


def main() -> None:
    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    global LOGGER
    LOGGER = logging.getLogger(__name__)
    reference()


if __name__ == "__main__":
    main()
