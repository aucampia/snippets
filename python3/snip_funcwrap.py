# mypy: warn-unused-configs, disallow-any-generics, disallow-subclassing-any
# mypy: disallow-untyped-calls, disallow-untyped-defs, disallow-incomplete-defs
# mypy: check-untyped-defs, disallow-untyped-decorators, no-implicit-optional,
# mypy: warn-redundant-casts, warn-unused-ignores, warn-return-any, no-implicit-reexport,
# mypy: strict-equality

import typing as typ
import functools
import functools as ft
import unittest.mock as utm
import unittest.mock
import os
import sys

import logging

logging.basicConfig(
    level=logging.INFO,
    stream=sys.stderr,
    datefmt="%Y-%m-%dT%H:%M:%S",
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)
LOGGER = logging.getLogger(__name__)

##############

GenericT = typ.TypeVar("GenericT")


def with_wrapper(base: GenericT, wrapper: GenericT) -> GenericT:
    base_ = typ.cast(typ.Any, base)
    wrapper_ = typ.cast(typ.Any, wrapper)

    @functools.wraps(base_)
    def wrapped(*args: typ.Any, **kwargs: typ.Any) -> typ.Any:
        return wrapper_(base, *args, **kwargs)

    return typ.cast(GenericT, wrapped)


def wrap_function(base: str) -> typ.Callable[[GenericT], GenericT]:
    basef = eval(base)

    def internal(wrapper: GenericT) -> GenericT:
        wrapped_basef = with_wrapper(basef, wrapper)
        setattr(wrapped_basef, "_org", basef)
        exec(f"{base} = wrapped_basef")
        return wrapper

    return internal


##############
class Dog:
    def __init__(self) -> None:
        pass

    def bark(self) -> str:
        return f"Dog.bark"


def test_wraps() -> None:
    @wrap_function("Dog.bark")
    def wrapper_function(base: typ.Callable[[str], str], arg: str) -> str:
        return f"wrapper_function({base(arg)!r})"

    assert Dog().bark() == r"wrapper_function('Dog.bark')"
