#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# vim: set filetype=python tw=100 cc=+1:
# pylint: disable=reimported,wrong-import-position
# pylint: disable=missing-docstring
# pylint: disable=bad-continuation
# pylint: disable=ungrouped-imports

# mypy: warn-unused-configs, disallow-any-generics, disallow-subclassing-any
# mypy: disallow-untyped-calls, disallow-untyped-defs, disallow-incomplete-defs
# mypy: check-untyped-defs, disallow-untyped-decorators, no-implicit-optional,
# mypy: warn-redundant-casts, warn-unused-ignores, warn-return-any, no-implicit-reexport,
# mypy: strict-equality

import typing
import logging
import enum

GenericT = typing.TypeVar("GenericT")

import inspect


def vdict(*keys: str, obj: typing.Any = None) -> typing.Dict[str, typing.Any]:
    if obj is None:
        lvars = typing.cast(typing.Any, inspect.currentframe()).f_back.f_locals
        return {key: lvars[key] for key in keys}
    return {key: getattr(obj, key, obj.get(key, None)) for key in keys}


def get_methods_class(
    method: typing.Callable[..., typing.Any]
) -> typing.Type[typing.Any]:
    methodx = typing.cast(typing.Any, method)
    if "__self__" in methodx and methodx.__self__:
        classes = methodx.__self__.__class__
    else:
        classes = methodx.im_class
    # assert "im_class" in methodx
    for cls in inspect.getmro(classes):
        if method.__name__ in cls.__dict__:
            return cls
    raise ValueError("Could not determine the class that defined method %s", method)


def static_init(cls: typing.Type[typing.Any]) -> typing.Type[typing.Any]:
    if getattr(cls, "static_init", None):
        typing.cast(typing.Any, cls).static_init()
    return cls


def test_static_init() -> None:
    @static_init
    class TestClassA:
        static_init_called = False

        @classmethod
        def static_init(cls) -> None:
            logging.info("entry ...")
            cls.static_init_called = True

    logging.info("TestClassA.__class__ = %s", TestClassA.__class__)
    logging.info("type(TestClassA) = %s", type(TestClassA))
    logging.info("inspect.isclass(TestClassA) = %s", inspect.isclass(TestClassA))
    logging.info(
        "inspect.ismethod(TestClassA.static_init) = %s",
        inspect.ismethod(TestClassA.static_init),
    )

    assert TestClassA.static_init_called is True


def enum_helper(cls: typing.Type[enum.Enum]) -> typing.Type[enum.Enum]:
    clsx = typing.cast(typing.Any, cls)
    setattr(cls, "values", set())
    setattr(cls, "value_map", dict())
    setattr(cls, "key_strings", set())
    for enm in cls:
        clsx.key_strings.add(enm.name)
        clsx.values.add(enm.value)
        if enm.value in clsx.value_map:
            raise RuntimeError(
                "enum %s value %s is not unique. Also shared by %s",
                enm,
                enm.value,
                clsx.value_map[enm.value],
            )
        clsx.value_map[enm.value] = enm
    return static_init(clsx)


def coalesce(*args: typing.Optional[GenericT]) -> typing.Optional[GenericT]:
    for arg in args:
        if arg is not None:
            return arg
    return None


def test_coalesce() -> None:
    assert coalesce(None, None, 111) == 111
    assert coalesce(None, 222, None) == 222
    assert coalesce(333, None, None) == 333
    assert coalesce(444) == 444
    assert coalesce(None) is None
    assert coalesce() is None


KeyT = typing.TypeVar("KeyT")
ValueT = typing.TypeVar("ValueT")


def coalesce_dict(
    dict_obj: typing.Dict[KeyT, ValueT],
    keys: typing.Collection[KeyT],
    check_value: bool = True,
) -> typing.Optional[ValueT]:
    for key in keys:
        if key in dict_obj and (not check_value or dict_obj[key]):
            return dict_obj[key]
    return None


def test_coalesce_dict() -> None:
    assert coalesce_dict({"a": 1, "b": 2, "c": 3}, ("aaa", "aa", "a")) == 1
    assert coalesce_dict({"a": 1, "b": 2, "c": 3}, ("c", "b", "a")) == 3


import collections


def deep_update(base: GenericT, update: GenericT) -> GenericT:
    base_ = typing.cast(typing.Any, base)
    update_ = typing.cast(typing.Any, update)
    for update_key, update_value in update_.items():
        if isinstance(update_value, collections.abc.Mapping):
            base_value = base_.get(update_key)
            if not isinstance(base_value, collections.abc.Mapping):
                base_value = {}
            base_[update_key] = deep_update(base_value, update_value)
        else:
            base_[update_key] = update_value
    return base


def test_deep_update() -> None:
    base: typing.Dict[typing.Any, typing.Any]
    update: typing.Dict[typing.Any, typing.Any]

    base = {"a": 1, "b": 2, "c": 3}
    update = {"a": 11, "b": {"ba": "_", "bb": "__"}, "c": 33}
    result = deep_update(base, update)
    assert result == update

    base = {"a": 11, "b": {"ba": "_", "bb": "__"}, "c": 33}
    update = {"a": 1, "b": 2, "c": 3}
    result = deep_update(base, update)
    assert result == update


import shlex


# https://github.com/python/cpython/blob/master/Lib/shlex.py#L318
def shlex_join(split_command: typing.List[str]) -> str:
    return " ".join(shlex.quote(arg) for arg in split_command)
