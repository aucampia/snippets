#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# vim: set filetype=python tw=100 cc=+1:

# mypy: warn-unused-configs, disallow-any-generics, disallow-subclassing-any
# mypy: disallow-untyped-calls, disallow-untyped-defs, disallow-incomplete-defs
# mypy: check-untyped-defs, disallow-untyped-decorators, no-implicit-optional,
# mypy: warn-redundant-casts, warn-unused-ignores, warn-return-any, no-implicit-reexport,
# mypy: strict-equality

import typing

# GenericT = typing.TypeVar("GenericT")


def static_init(cls: typing.Type[typing.Any]) -> typing.Type[typing.Any]:
    if getattr(cls, "static_init", None):
        typing.cast(typing.Any, cls).static_init()
    return cls


import logging


def test_static_init() -> None:
    @static_init
    class TestClassA:
        static_init_called = False

        @classmethod
        def static_init(cls) -> None:
            logging.info("entry ...")
            cls.static_init_called = True

    logging.info("TestClassA.__class__ = %s", TestClassA.__class__)
    logging.info("type(TestClassA) = %s", type(TestClassA))
    logging.info("inspect.isclass(TestClassA) = %s", inspect.isclass(TestClassA))
    logging.info(
        "inspect.ismethod(TestClassA.static_init) = %s",
        inspect.ismethod(TestClassA.static_init),
    )

    assert TestClassA.static_init_called is True


import inspect


def get_methods_class(
    method: typing.Callable[..., typing.Any]
) -> typing.Type[typing.Any]:
    methodx = typing.cast(typing.Any, method)
    if hasattr(methodx, "__self__") and methodx.__self__:
        classes = methodx.__self__.__class__
    else:
        classes = methodx.im_class
    # assert "im_class" in methodx
    for cls in inspect.getmro(classes):
        if method.__name__ in cls.__dict__:
            return cls
    raise ValueError("Could not determine the class that defined method %s", method)


def static_init_next(
    target: typing.Union[
        typing.Type[typing.Any], typing.Callable[[typing.Type[typing.Any]], None]
    ]
) -> typing.Any:
    logging.debug("target = %s", target)
    logging.debug("inspect.isclass(%s) = %s", target, inspect.isclass(target))
    logging.debug("inspect.ismethod(%s) = %s", target, inspect.ismethod(target))
    logging.debug("inspect.isfunction(%s) = %s", target, inspect.isfunction(target))
    if inspect.isclass(target):
        logging.debug("(class) target = %s", target)
        if getattr(target, "static_init", None):
            typing.cast(typing.Any, target).static_init()
    elif inspect.isfunction(target):
        logging.debug("(method) target = %s", target)
        logging.debug("target.__module__ = %s", target.__module__)
        logging.debug("target.__qualname__ = %s", target.__qualname__)
        typed_target = typing.cast(
            typing.Callable[[typing.Type[typing.Any]], None], target
        )
        cls = get_methods_class(typed_target)
        logging.info("cls = %s", cls)
        typed_target(cls)
    return target


def test_static_init_next() -> None:
    class TestClassB:
        static_init_called = False

        @classmethod
        # @static_init_next
        def init_me(cls) -> None:
            logging.info("entry ...")
            cls.static_init_called = True

    logging.info(
        "inspect.ismethod(TestClassB.init_me) = %s",
        inspect.ismethod(TestClassB.init_me),
    )

    logging.info(
        "inspect.ismethod(TestClassB.init_me) = %s",
        inspect.ismethod(TestClassB.init_me),
    )

    # assert TestClassB.static_init_called is True


if __name__ == "__main__":
    import pytest

    pytest.main([__file__, "--capture=no", "--log-cli-level=DEBUG"])
