#!/usr/bin/env python3
# vim: set filetype=python tw=100 cc=+1:

# python3 -m pytest --log-cli-level=DEBUG snip-textwrap.py
# pydoc3 file_uri_to_path

# https://www.gnu.org/software/coreutils/manual/html_node/fold-invocation.html#fold-invocation

import logging
import sys

logging.basicConfig(
    level=logging.INFO,
    datefmt="%Y-%m-%dT%H:%M:%S",
    stream=sys.stderr,
    format=(
        "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
        "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
    ),
)

logger = logging.getLogger(__name__)

import textwrap


def unwrap_text(text):
    return "\n\n".join(unwrap_text_to_paragraphs(text))


def unwrap_text_to_paragraphs(text):
    paragraphs = [""]
    lines = text.splitlines()
    line_previous = None
    for line in lines:
        if (
            line_previous is not None
            and (not line_previous or line_previous.isspace())
            and paragraphs[-1]
        ):
            paragraphs.append("")
        line_previous = line
        if not line:
            continue
        if paragraphs[-1]:
            paragraphs[-1] += " "
        paragraphs[-1] += line.rstrip()
    return paragraphs


def wrap_paragraphs(paragraphs, width):
    if width is None:
        return paragraphs
    return ["\n".join(textwrap.wrap(paragraph, width)) for paragraph in paragraphs]


def wrap_text(text, width):
    paragraphs = unwrap_text_to_paragraphs(text)
    return "\n\n".join(wrap_paragraphs(paragraphs, width))


import os
import pytest


def test_unwrap_text_to_paragraphs():
    # curl --silent http://metaphorpsum.com/paragraphs/4
    paragraphs = [
        "They were lost without the wifeless chill that composed their flood. Nowhere is it disputed that a smash is a tennis from the right perspective. In ancient times one cannot separate mints from unsquared pediatricians.",
        "In recent years, before appendixes, dramas were only maies. Extending this logic, an interactive is a lying crate. This could be, or perhaps a dancing doctor is a tailor of the mind.",
        "In modern times a hunchbacked cicada without restaurants is truly a fiberglass of ungloved frowns. A pencil sees a danger as a commo wax. We know that the powder of a plow becomes a templed breakfast.",
        "Though we assume the latter, some wailful inventions are thought of simply as peonies. One cannot separate networks from unwitched freons. A reading is an otic pair.",
    ]

    wrapped = "\n\n".join(
        ["\n".join(textwrap.wrap(paragraph, 60)) for paragraph in paragraphs]
    )

    logger.debug("wrapped = %s", wrapped)

    unwrapped = unwrap_text_to_paragraphs(wrapped)

    logger.debug("unwrapped = %s", unwrapped)

    assert unwrapped == paragraphs


def test_unwrap_text():
    # curl --silent http://metaphorpsum.com/paragraphs/4
    paragraphs = [
        "Pictures are darkling colds. A breakfast of the sail is assumed to be an alar pond. We can assume that any instance of a stream can be construed as a sunlit pediatrician.",
        "Nowhere is it disputed that the branchless cougar reveals itself as a nervine cartoon to those who look. A trickless octopus's november comes with it the thought that the headmost pet is a chime. A bratty waitress's caution comes with it the thought that the longer uncle is a space. A pass breakfast without comparisons is truly a measure of lanose mascaras. Some loudish texts are thought of simply as cares.",
        "Before secretaries, coppers were only employers. Few can name a cestoid vegetable that isn't a faithful tip. However, the first blockish tuna is, in its own way, a rugby.",
        "Nowhere is it disputed that okras are unsoft diggers. However, a pimple is an underpant from the right perspective. Some assert that authors often misinterpret the mall as a starveling advertisement, when in actuality it feels more like a fitful policeman. The collars could be said to resemble produced coffees.",
    ]

    wrapped = "\n\n".join(
        ["\n".join(textwrap.wrap(paragraph, 60)) for paragraph in paragraphs]
    )

    logger.debug("wrapped = %s", wrapped)

    unwrapped = unwrap_text(wrapped)

    logger.debug("unwrapped = %s", unwrapped)

    assert unwrapped == "\n\n".join(paragraphs)


def test_wrap_text():
    # { curl --silent http://metaphorpsum.com/paragraphs/4; echo; } | tee /dev/stderr | sponge | fold -w 60 -s | sed -E 's/\s*$//g'

    unwrapped = """A desmoid agreement is a cherry of the mind. A voiceless january without raviolis is truly a ethiopia of seral compositions. A homey vacation without tons is truly a chronometer of rebuked brackets. Some posit the shalwar grain to be less than weepy. A heart is an unperched vegetarian.

Stems are unclaimed shovels. What we don't know for sure is whether or not before laces, mountains were only perus. The heartsome hemp reveals itself as a monger pyjama to those who look. If this was somewhat unclear, some posit the coatless lunchroom to be less than stilted. This is not to discredit the idea that a tip sees an abyssinian as a legless tom-tom.

The chalks could be said to resemble undipped irons. Few can name a conoid perfume that isn't an unbowed copyright. Their sweater was, in this moment, a warlike fender. Far from the truth, their eggnog was, in this moment, an untrimmed edge.

To be more specific, a grain can hardly be considered a ramose daughter without also being a flood. As far as we can estimate, a bolt of the donna is assumed to be a gular timbale. A mangy pest's violet comes with it the thought that the dowdy geometry is an edger. Retained activities show us how ashtraies can be taxis."""

    wrapped = """A desmoid agreement is a cherry of the mind. A voiceless
january without raviolis is truly a ethiopia of seral
compositions. A homey vacation without tons is truly a
chronometer of rebuked brackets. Some posit the shalwar
grain to be less than weepy. A heart is an unperched
vegetarian.

Stems are unclaimed shovels. What we don't know for sure is
whether or not before laces, mountains were only perus. The
heartsome hemp reveals itself as a monger pyjama to those
who look. If this was somewhat unclear, some posit the
coatless lunchroom to be less than stilted. This is not to
discredit the idea that a tip sees an abyssinian as a
legless tom-tom.

The chalks could be said to resemble undipped irons. Few can
name a conoid perfume that isn't an unbowed copyright. Their
sweater was, in this moment, a warlike fender. Far from the
truth, their eggnog was, in this moment, an untrimmed edge.

To be more specific, a grain can hardly be considered a
ramose daughter without also being a flood. As far as we can
estimate, a bolt of the donna is assumed to be a gular
timbale. A mangy pest's violet comes with it the thought
that the dowdy geometry is an edger. Retained activities
show us how ashtraies can be taxis."""

    assert wrapped == wrap_text(unwrapped, 60)
    assert unwrapped == wrap_text(unwrapped, None)
