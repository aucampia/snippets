#!/usr/bin/env python3
# vim: set filetype=python sts=4 ts=4 sw=4 expandtab tw=100 cc=+1:
# vim: set filetype=python tw=100 cc=+1:
# pylint: disable=reimported,wrong-import-position
# pylint: disable=missing-docstring
# pylint: disable=bad-continuation
# pylint: disable=ungrouped-imports

"""
This module is boilerplate for a python CLI script.
"""

# python3 -m pylint --rcfile=/dev/null boilerplate.py

import logging

LOGGER = logging.getLogger(__name__)

import os.path

SCRIPT_DIRNAME = os.path.dirname(__file__)
SCRIPT_DIRNAMEABS = os.path.abspath(SCRIPT_DIRNAME)
SCRIPT_BASENAME = os.path.basename(__file__)

import pathlib

SCRIPT_PATH = pathlib.Path(__file__)

import sys
import argparse
from contextlib import contextmanager
import types


def collate(*args):
    for arg in args:
        if arg is not None:
            return arg
    return None


class Application:
    def __init__(self, parser=None):
        LOGGER.info("entry ...")
        self.parse_result = None
        self.args = None
        self.parser = collate(parser)
        self._do_init(parser)

    def _do_init(self, parser=None):
        LOGGER.debug("entry ...")
        self.parser = collate(parser, self.parser)
        if self.parser is None:
            own_parser = True
            self.parser = argparse.ArgumentParser(add_help=True)
        else:
            own_parser = False
        parser = self.parser
        if own_parser:
            parser.add_argument(
                "-v",
                "--verbose",
                action="count",
                dest="verbosity",
                help="increase verbosity level",
            )
        parser.set_defaults(handler=self.handle)
        apc = types.SimpleNamespace(parsers=[parser], subparsers=[])
        apc.subparsers.append(
            apc.parsers[-1].add_subparsers(dest=f"subparser_{len(apc.subparsers)}")
        )

        def nesti(index, name, parser_args={}, subparsers_args={}):
            assert index == len(apc.subparsers)
            apc.parsers.append(apc.subparsers[-1].add_parser(name, **parser_args))
            apc.subparsers.append(
                apc.parsers[-1].add_subparsers(
                    dest=f"subparser_{len(apc.subparsers)}", **subparsers_args
                )
            )

        def nestd(index):
            assert index == (len(apc.subparsers) - 1)
            apc.subparsers.pop()
            apc.parsers.pop()

        nesti(1, "s1")
        nesti(2, "s1.1")
        nesti(3, "s1.1.1")
        nestd(3)
        nestd(2)
        nesti(2, "s1.2")
        nestd(2)
        nestd(1)
        nesti(1, "s2")
        nestd(1)

        @contextmanager
        def new_subparser(name, parser_args={}, subparsers_args={}):
            apc.parsers.append(apc.subparsers[-1].add_parser(name, **parser_args))
            apc.subparsers.append(
                apc.parsers[-1].add_subparsers(
                    dest=f"subparser_{len(apc.subparsers)}", **subparsers_args
                )
            )
            try:
                yield apc.parsers[-1]
            finally:
                apc.subparsers.pop()
                apc.parsers.pop()

        with new_subparser("s3") as parser:
            with new_subparser("s3.1") as parser:
                with new_subparser("s3.1.1") as parser:
                    parser.add_argument("--what")
            with new_subparser("s3.2") as parser:
                with new_subparser("s3.2.1") as parser:
                    parser.add_argument("--what")

    def _parse_args(self, args=None):
        LOGGER.debug("entry ...")
        self.args = collate(args, self.args, sys.argv[1:])
        self.parse_result = self.parser.parse_args(self.args)

        verbosity = self.parse_result.verbosity
        if verbosity is not None:
            root_logger = logging.getLogger("")
            root_logger.propagate = True
            new_level = (
                root_logger.getEffectiveLevel()
                - (min(1, verbosity)) * 10
                - min(max(0, verbosity - 1), 9) * 1
            )
            root_logger.setLevel(new_level)

        LOGGER.debug(
            "args = %s, parse_result = %s, logging.level = %s, LOGGER.level = %s",
            self.args,
            self.parse_result,
            logging.getLogger("").getEffectiveLevel(),
            LOGGER.getEffectiveLevel(),
        )

        if "handler" in self.parse_result and self.parse_result.handler:
            self.parse_result.handler(self.parse_result)

    def do_invoke(self, args=None):
        self._parse_args(args)

    # parser is so this can be nested as a subcommand ...
    @classmethod
    def invoke(cls, *, parser=None, args=None):
        app = cls(parser)
        app.do_invoke(args)

    def handle(self, parse_result=None):
        LOGGER.debug("entry ...")
        self.parse_result = parse_result = collate(parse_result, self.parse_result)

        LOGGER.info("stuff to do goes here ...")


def main():
    logging.basicConfig(
        level=logging.INFO,
        stream=sys.stderr,
        datefmt="%Y-%m-%dT%H:%M:%S",
        format=(
            "%(asctime)s %(process)d %(thread)d %(levelno)03d:%(levelname)-8s "
            "%(name)-12s %(module)s:%(lineno)s:%(funcName)s %(message)s"
        ),
    )

    Application.invoke()


if __name__ == "__main__":
    main()
