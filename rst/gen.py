#!/usr/bin/env python3

import docutils.core
docutils.core.publish_file(
    source_path="reference.rst",
    destination_path="reference.html",
    writer_name="html"
)
