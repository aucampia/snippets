================
Doc Title
================
----------------
Doc Subtitle
----------------

.. comment:
   * A ReStructuredText Primer: http://docutils.sourceforge.net/docs/user/rst/quickstart.html
   * source: https://repo.or.cz/docutils.git/blob/HEAD:/docutils/docs/user/rst/quickstart.txt
   * Quick reStructuredText: http://docutils.sourceforge.net/docs/user/rst/quickref.html
   * The reStructuredText_ Cheat Sheet: Syntax Reminders: http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt
   * An Introduction to reStructuredText: http://docutils.sourceforge.net/docs/ref/rst/introduction.html
   * reStructuredText Markup Specification: http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html
   * reStructuredText Directives: http://docutils.sourceforge.net/docs/ref/rst/directives.html
   * reStructuredText Interpreted Text Roles: http://docutils.sourceforge.net/docs/ref/rst/roles.html


..
    :depth: 5

.. sectnum::
.. contents::




..  .. .. ..
    http://docutils.sourceforge.net/rst.html



.. s/\(.*\)\[\s*\(\S.*\S\)\s*\]/`\2 <\1>`_/gc

+ `reStructuredText <http://docutils.sourceforge.net/rst.html>`_

    + `A ReStructuredText Primer <http://docutils.sourceforge.net/docs/user/rst/quickstart.html>`_
      `source <https://repo.or.cz/docutils.git/blob/HEAD:/docutils/docs/user/rst/quickstart.txt>`_
    + `Quick reStructuredText <http://docutils.sourceforge.net/docs/user/rst/quickref.html>`_
    + `The reStructuredText_ Cheat Sheet: Syntax Reminders <http://docutils.sourceforge.net/docs/user/rst/cheatsheet.txt>`_
    + `An Introduction to reStructuredText <http://docutils.sourceforge.net/docs/ref/rst/introduction.html>`_
    + `reStructuredText Markup Specification <http://docutils.sourceforge.net/docs/ref/rst/restructuredtext.html>`_
    + `reStructuredText Directives <http://docutils.sourceforge.net/docs/ref/rst/directives.html>`_
    + `reStructuredText Interpreted Text Roles <http://docutils.sourceforge.net/docs/ref/rst/roles.html>`_

+ `Restructured Text (reST) and Sphinx CheatSheet <https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html>`_

Section Title
=============

Section Title
-------------

Section Title
`````````````

Section Title
'''''''''''''

Section Title
.............

Section Title
~~~~~~~~~~~~~

Section Title
*************

Section Title
+++++++++++++

Section Title
^^^^^^^^^^^^^
