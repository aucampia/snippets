#!/usr/bin/env ruby

# https://www.ruby-lang.org/en/documentation/
# https://ruby-doc.org/core/

# site:docs.ruby-lang.org/en/master/
# https://docs.ruby-lang.org/en/master/
# https://docs.ruby-lang.org/en/master/table_of_contents.html

require 'logger'
require 'optparse'

# https://docs.ruby-lang.org/en/master/Logger.html
# https://docs.ruby-lang.org/en/master/Proc.html
$logger = Logger.new(STDERR, level: Logger::DEBUG, formatter: proc {|severity, datetime, progname, msg|
  # https://docs.ruby-lang.org/en/master/syntax/literals_rdoc.html#label-Strings
  # https://docs.ruby-lang.org/en/master/DateTime.html#method-i-strftime
  "#{datetime.strftime("%Y-%m-%dT%H:%M:%S")} #{Process.pid} #{Thread.current.object_id} #{progname} #{severity} #{msg}\n"
})

# https://docs.ruby-lang.org/en/master/syntax/assignment_rdoc.html
# local
# @instance
# @@class
# $global

# https://docs.ruby-lang.org/en/master/syntax/modules_and_classes_rdoc.html
# https://docs.ruby-lang.org/en/master/BasicObject.html
# https://docs.ruby-lang.org/en/master/Object.html
# https://docs.ruby-lang.org/en/master/Class.html
class MyClass
  # https://docs.ruby-lang.org/en/master/syntax/modules_and_classes_rdoc.html#label-Singleton+Classes
  class << self
    def static_method_a()

    end
  end
  def self.static_method_b()

  end
end

class << MyClass
    def static_method_c()

    end
end

class Application
  # https://docs.ruby-lang.org/en/master/OptionParser.html
  class << self
    def run_main(argv: nil)
      $logger.debug("#{__FILE__}:#{__LINE__} #{self.name}:#{__method__}: entry")
      Application.new().main(argv: argv)
    end
  end

  # https://docs.ruby-lang.org/en/master/Class.html#method-i-new
  def initialize()

  end
  def main(argv: nil)
    $logger.debug("#{__FILE__}:#{__LINE__} #{self.class.name}:#{__method__}: entry")
    begin
      # https://docs.ruby-lang.org/en/master/OptionParser.html
      if argv.nil?
        argv = ARGV
      end

      # https://docs.ruby-lang.org/en/master/syntax/literals_rdoc.html#label-Symbols
      options = {
        verbosity: 0
      }
      option_parser = OptionParser.new do |opts|
        opts.on('-v', '--verbose') do |v|
          options[:verbosity] += 1
        end
      end
      $logger.debug("#{__FILE__}:#{__LINE__} #{self.class.name}:#{__method__}: will parse #{argv}")
      option_parser.parse!(into: options)
      $logger.debug("#{__FILE__}:#{__LINE__} #{self.class.name}:#{__method__}: options = #{options}")


      $logger.info("#{__FILE__}:#{__LINE__} #{self.class.name}:#{__method__}: main ...")
    rescue => exception
      $logger.fatal("Caught exception; exiting")
      $logger.fatal(exception)
      exception.backtrace.map{|line| $logger.fatal(line)}
    end
  end
end

# https://docs.ruby-lang.org/en/2.5.0/syntax/methods_rdoc.html
def main(argv: nil)
  # https://docs.ruby-lang.org/en/master/keywords_rdoc.html
  $logger.debug("#{__FILE__}:#{__LINE__} #{self.class.name}:#{__method__}: entry")
  Application.run_main(argv: argv)
end

# https://docs.ruby-lang.org/en/master/syntax/control_expressions_rdoc.html

# https://docs.ruby-lang.org/en/master/syntax/exceptions_rdoc.html

# https://docs.ruby-lang.org/en/master/syntax/calling_methods_rdoc.html
# https://docs.ruby-lang.org/en/master/ARGF.html
main(argv: ARGV)
